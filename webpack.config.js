const path = require('path');
const htmlwebplugin=require('html-webpack-plugin')
module.exports = {
  mode: 'development',
  entry: path.join(__dirname, 'app/index.js'),
  output: {
    path:path.join(__dirname, 'public/dist'),
    publicPath: '/public/dist/',
    filename: "bundle.js",
    chunkFilename: '[name].js'
  },
  module: {
    rules: [{
      test: /.jsx?$/,
      include: [
        path.resolve(__dirname, 'app')
      ],
      exclude: [
        path.resolve(__dirname, 'node_modules')
      ],
      loader: 'babel-loader',
      query: {
        presets: [
          ["@babel/env", {
            "targets": {
              "browsers": "last 2 chrome versions"
            }
          }]
        ]
      }
    }]
  },
  resolve: {
    extensions: ['.json', '.js', '.jsx']
  },
  plugins:[
    new htmlwebplugin({
      title:'React App',
      filename: path.join(__dirname,'public/index.html'),
      template: path.join(__dirname,'app/index.html')
    })
  ],
  devtool: 'source-map',
  devServer: {
    contentBase: path.join(__dirname,'public'),
    inline: true,
    host: 'localhost',
    port: 3300,
  }
};