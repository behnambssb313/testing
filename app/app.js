import React from "react";
import Form from './Form'


class App extends React.Component {
    constructor(props){
        super(props);
        this.state={
            appp:[
                {
                name:'behnam',
                family:'sarkhosh'
            },
            {
                name:'behnam1',
                family:'sarkhosh2'
            }
        ]
        };
    }

    handler=(item)=>{
        this.setState({
            ...this.state,
            appp:[...this.state.appp,item]
        });
       console.log(this.state.appp)
    };

    render() {
        return (
            <div>
                {this.state.appp.map(item=>{
                   return <div>
                        <p>{item.name} {item.family}</p>
                   </div>
                })}
                <Form onSaved={this.handler}/>
            </div>
        )
    }
}

export default App;

